#!/bin/bash
lspci -nn | grep VGA
apt-get install -y linux-headers-$(uname -r)
apt-get install nvidia-kernel-$(uname -r)
apt-get install nvidia-kernel-dkms
aptitude install nvidia-xconfig
sed 's/quiet/quiet nouveau.modeset=0/g' -i /etc/default/grub
update-grub
glxinfo | grep -i "direct rendering"
nvidia-xconfig
aptitude install mesa-utils
lsmod | grep nvidia
lsmod | grep nouveau
