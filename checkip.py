#!/usr/bin/python
'''

IP check on Basesender 
List out the reputation

'''

__version__ = '1.0'
__RFC__ = 'RFC6471'

try:
	import urllib,urllib2
	import dns.resolver
	import datetime
	import re
	import sys
	import time
	import commands
	from cookielib import MozillaCookieJar
	from bs4 import BeautifulSoup
		
except:
	print "Exit. Failed to load the Python modules ++++++++"
	# print sys.exc_info()[1]
	sys.exit(0)

class IPCHECK:
	def __init__(self,iplist):
		# print type(self.iplist)
		# assert isinstance(iplist, list), "Provide a list of IP addresses"
		self.iplist = iplist
		self.ts = time.time()

	# def __IP__(self,iplist):
	# 	self.iplist = iplist

	def printplus(self, msg):
		pref = '+++++'
		print "%-15s%s%15s" % (pref , msg, pref) 

	

	def browser_setup(self, url):
		
		cf = MozillaCookieJar('cookies.txt')
		cf.load()
		opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cf), urllib2.HTTPHandler())
		# opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36')]
		# opener.addheader = [('Origin', 'http://www.senderbase.org')]
		# opener.addheader = [('Referer', 'http://www.senderbase.org/')]
		# opener.addheader = [('Cookie', '__utmt=1')]
		# opener.addheader = [('Cookie', '__utma=175757344.1074631397.1415580183.1415580183.1415580183.1')]
		# opener.addheader = [('Cookie', ' __utmb=175757344.4.10.1415580183')]
		# opener.addheader = [('Cookie', '__utmz=175757344.1415580183.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)')]
		s = opener.open(url)
		html = s.read()
		st = datetime.datetime.fromtimestamp(self.ts).strftime('%Y-%m-%d %H:%M:%S')
		with open('temp.dat','w') as f:
			f.write(html)
			f.write(st)
		f.close()
		s.close()
		return html

	def check_reputuationauthority(self):
		self.reputation_status = {}
		ip = self.iplist

		url = "http://reputationauthority.org/lookup.php?ip=%s&Submit.x=17&Submit.y=16&Submit=Search" % (ip)
		if ip:
			html = self.browser_setup(url)
			soup = BeautifulSoup(html)
			
			self.printplus("ReputuationAuthority_org")
			print "Title: %10s" % soup.title.string
			print "Reputation Score: %7s" % soup.find('td', class_="bsnDataRow2").string
		pass


	def check_dnsbl(self):
		self.dnsbl_status = {}

		url = "http://www.dnsbl.info/dnsbl-database-check.php"
		

		ip = '.'.join(self.iplist.split('.')[::-1])
		print ip

		if ip:
			html = self.browser_setup(url)
			soup = BeautifulSoup(html)
			self.printplus("DNSBL Checking...")
			print "Title %10s" % soup.title.string
			links = soup.find_all('a', {'href': re.compile('php\?dnsbl')})
			for li in links:
				# print 'Checking on ... : ' + li.text
				rev_name = ip + '.' + li.text
				# print rev_name
				# query  = socket.gethostbyaddr(rev_name)
				mresolver = dns.resolver.Resolver()
				try:
					query = mresolver.query(rev_name, 'TXT')

					if query.response:
						self.printplus("Blacklisted by - %s" % li.text)
				except:
					pass


		pass


	def check_senderbase(self):
		self.senderbase_status = {}

		ip = self.iplist
		if ip:
		# for ip in self.iplist:
			# print type(self.iplist) 
			# print self.iplist
		# if self.iplist:
			url = 'http://www.senderbase.org/lookup/?search_string=%s' % (ip)
			html = self.browser_setup(url)
			soup = BeautifulSoup(html)

			self.printplus("SenderBase Checking")
			
			print "Title: %10s" % soup.title.string 
			# tag = soup.div
			# type(tag)
			# tag['class'] = 'leftside'
			# print tag
			print "Email Reputation : " + soup.find('div', class_="leftside").string
			# s = urllib.urlopen(url)
			# html = s.read()
			bls = soup.find_all('td', class_='bl_header')
			for bl in bls:
				outcome = bl.find_next_sibling('td').text
				print "%10s - %10s" % (bl.text, outcome)


			# regex = re.compile("""SenderBase reputation score""" + \
                                       # """</td>.*?<td[a-zA-Z="%0-9' ]*>""" + \
                                       # """.*?([a-zA-Z]+).*?</td>""",
                                       # re.I | re.S | re.M)
			# ouput = regex.findall(html)

			try:
				self.senderbase_status[ip] = output[0]
			except:
				pass



class DNSCHECK:
	def __init__(self):
		# self.iplist = []
		self.iplist = sys.argv[1]
		self.senderbase_check = 'True'
		self.reputationauthority_check = "True"
		self.dnsbl_check = 'True'


	def run(self):
		# self.iplist.append(sys.argv[1].split(","))
		ip_check = IPCHECK(self.iplist)

		if self.senderbase_check == 'True':
			ip_check.check_senderbase()

		if self.reputationauthority_check == 'True':
			ip_check.check_reputuationauthority()

		if self.dnsbl_check == 'True':
			ip_check.check_dnsbl()
		pass
		
			
if __name__ == "__main__":
		if len(sys.argv) != 2:
			print "Usage: python checkip.py 101.x.x.x"
			sys.exit(0)

		dnscheck = DNSCHECK()
		dnscheck.run()
